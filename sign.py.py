from Crypto.Signature import PKCS1_v1_5
from Crypto.Hash import SHA256
from Crypto.Public import RSA
import codecs

key=RSA.generate(1024)
private_key=key.exportKey()
public_key=key.public.exportKey()

public_key="""----BEGIN PUBLIC KEY-----
  MIICIjANBgkqhkiG9w0BAQEFAAOCAg8AMIICCgKCAgEA0TqlveKKlc2MFvEmuXJi
  LGBsY1t4ML4uiRADGSZlnc+7Ugv3h+MCjkkwOKiOdsNo8k4KSBIG5GcQfKYOOd17
  AJvqCL6cGQbaLuqv0a64jeDm8oO8/xN/IM0oKw7rMr/2oAJOgIsfeXPkRxWWic9A
  VIS++H5Qi2r7bUFX+cqFsyUCAwEAAQ
  -----END PUBLIC KEY-----"""

private_key="""-----BEGIN RSA PRIVATE KEY-----
7bjmSrSyOI0sBc4ptqvIHGPPhQb0KIGxKK6vFE0ASlZQRbUHZyEossVKbTw0Mzha
YP+8tBNiMu/vME3Z30Va6skK+RGM8eutem0EcAe1SdCxtB/VCT0yaYQii2H8D71u
dG2gvbk4N7cF1P09GWBpj5W5+LP2Bv+u6njifJ40pEdypLOY5BC3n8dGMvPFfNaB
LEbB5eZZPPnBONchhcpvFuQrqEomBLd5wa6jNAnfcx34TggjGEJWx55ccEtrn/Bh
MTtPxVHOlF8+JMDLPt/4sfS6zIGQNShj0VBsXcoXJlL+h0BNzUz+ibSmLjPs+QUo
bqPRIBZML5WKNdCUSVPPnceOv3e8HvlLQKg5HZHdy1biiWrDNReDtkOJayopqla7
zBnoTnuVnWp/AYiEbgI2uX+q41/d2ttaerFgtIGAKYjZp/p8WXpwypmMNDf9Letk
zKLZUb0fersmpKUE2SZxvvHP5xPj2XtlypL7wuXwR37h9W8tUScNTJVmBLUud5Rt
L4YsaOcDCazrHVMmQVJUfLv3X7jh0QPpGnrCx3LxH6cvPEBwRZnCwOu3/YtWda0E
NcVREIP5evUAArDiATnu0nBkZDadBmTYqjMMXdloTkMQ5ivM/8gr26672mFD1TYY
O7GGsIgisVc01DVWwnvZAAy0DoGITkKRSVEraLRig61dU/OAZRffpTiU1OiTiG5b
WjUr2oksktCZcNBBNBRWd5W2d1P1bHgopD7GsuxYnl5fKAO9AgQHv0IO5qnyLoV0
hfDHPckkeQx5FOeqWMPXMb9rCcqml97ZwqcUVXTGowSc69ODa3vVmQub+S/t5oq/
qAuP4q0Y+f90jRNR6Fnph2b0iH+2l1A8/MaIm5giHNGP97caPKHyPf/7bs8pK2J+
DRxXHJ1iT2mR3YN0VC7u8cwoGhbhVv88yX+Bv56H80XLUUdKQQuFoPEt6n53oXAr
zuEZjjqdBmnIBSprJpY27e7ErWDdL+XOG8WKuBhkXcXEGbi+h/sd23ZI1Z9Xe2Gy
dmP0ho0pc8GfKbv5fh3bdgN09ARVv3JJUu03rs6T+9HOpjLtzf189Ol5QL0SqRHo
SNl0vO2nZoPf2bXHXZcH6yK/XIIP+TbNu5uHyY8gK01Tv0O9SPQ+SE8ZMiTou/Rm
+TQxd07Oe663O7l0lHb7SpAvhOyZftiga+Jmq87UJrbRC9gWustkMpi7436J6QbY
Sq1vcAKU/EiQkSiiqEokGXdGeU1SR7uPassMzO7p2lTglfiNMOA+nq0Rp9M9raTq
6wRPTkIDfKILsLcUaCRnJBqdVjGFg1zgceVthBBZXw/okHKshhfNIBTIia+XNUYt
2MOhnoptglZt2aWk8MPS/Fav18nuJR1YjoHfwSEFX6n1gnVh8XblXav6SXJY2+uW
/nm9H5m0yGEcuWoN8n85ERIt5LN1vuUkMj79qa9+jSTwzd1g8qmipPExUHJHNon1
nttCc3el0OjqEGbvWPBTGmuOGAigkp0wQBaUJbd0T2o5wCw+eBHnLLaA5BmUJHtO
GPIOYqh69o0EccfLlC21pjH6u7+LvB7JjqZWAaTuLszpoyBPBJ+x8Q==
-----END RSA PRIVATE KEY-----"""
print (private_key)
message="HELLO WORLD"
h=SHA256.new(message)

priv_key=RSA.importKey(private_key)
pub_key=RSA.importKey(public_key)

singer=PKCS1_v1_5.new(priv_key)
signature=singer.sign(h)
print(signature)
hexify=codecs.getdencoder('hex')
m=hexify(signature)[0]
print(m)
